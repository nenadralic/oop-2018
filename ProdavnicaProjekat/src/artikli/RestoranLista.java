package artikli;

public class RestoranLista {
	private String restoran;
	private String adresa;
	private String vrstaKuhinje;
	
	public RestoranLista() {
		this.restoran = "";
		this.adresa = "";
		this.vrstaKuhinje = "";
	}
	
	public RestoranLista(String restoran, String adresa, String vrstaKuhinje) {
		super();
		this.restoran = restoran;
		this.adresa = adresa;
		this.vrstaKuhinje = vrstaKuhinje;
	}

	public RestoranLista(RestoranLista original) {
		this.restoran = original.restoran;
		this.adresa = original.adresa;
		this.vrstaKuhinje = original.vrstaKuhinje;
	}

	public String getRestoran() {
		return restoran;
	}

	public void setRestoran(String restoran) {
		this.restoran = restoran;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getVrstaKuhinje() {
		return vrstaKuhinje;
	}

	public void setVrstaKuhinje(String vrstaKuhinje) {
		this.vrstaKuhinje = vrstaKuhinje;
	}

	@Override
	public String toString() {
		return "Restoran [restoran=" + restoran + ", adresa=" + adresa + ", vrstaKuhinje=" + vrstaKuhinje + "]";
	}
}
