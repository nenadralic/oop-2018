package guiIzmenaiDodavanje;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import artikli.Artikli;
import artikli.Narudzbina;
import korisnici.Korisnici;
import net.miginfocom.swing.MigLayout;
import restoran.Restoran;

public class NarudzbinaFormaKupacIzmena extends JFrame {
	private JLabel lblJelo = new JLabel("Jelo");
	private JTextField txtJelo = new JTextField(20);
	private JLabel lblPice = new JLabel("Pice");
	private JTextField txtPice = new JTextField(20);
	private JLabel lblCena = new JLabel("Cena");
	private JTextField txtCena = new JTextField(20);
	private JLabel lblStatus = new JLabel("Status");
	private JComboBox<String> cbStatus = new JComboBox<>();
	private JLabel lblDatum = new JLabel("Datum");
	private JTextField txtDatum = new JTextField(20);
	private JLabel lblKupac = new JLabel("Kupac");
	private JTextField txtKupac = new JTextField(20);
	private JLabel lblDostavljac = new JLabel("Dostavljac");
	private JTextField txtDostavljac = new JTextField(20);
	
	
	
	
	private JButton btnOk = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	
	private Restoran restoran;
	private Korisnici prijavljeniKorisnik;
	private Narudzbina narudzbina;
	
	
	public NarudzbinaFormaKupacIzmena(Restoran restoran, Korisnici prijavljeniKorisnik, Narudzbina narudzbina) {
		this.restoran = restoran;
		this.prijavljeniKorisnik = prijavljeniKorisnik;
		this.narudzbina = narudzbina;
		if(this.prijavljeniKorisnik == null) {
			setTitle("Dodavanje artikala");
		}else {
			setTitle("Izmena podataka - " + this.prijavljeniKorisnik.getKorIme());
		}
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		initGUI();
		initActions();
		setResizable(false);
		pack();

	}
	private void initGUI() {
		MigLayout layout = new MigLayout("wrap 2");
		setLayout(layout);
		
		
	
		enablePolja(true);					
		if(this.narudzbina != null) {
			txtJelo.setText(this.narudzbina.getJelo());
			txtPice.setText(this.narudzbina.getPice());
			txtCena.setText(String.valueOf(this.narudzbina.getCenaPorudzbine()));
			txtDatum.setText(this.narudzbina.getVremeiDatum());
			txtKupac.setText(this.narudzbina.getKupac());
			txtDostavljac.setText(this.narudzbina.getDostavljac());
			
		}
		
		cbStatus.addItem("Otkazana");
		
		add(lblJelo);
		add(txtJelo);
		add(lblPice);
		add(txtPice);
		add(lblDatum);
		add(txtDatum);
		add(lblKupac);
		add(txtKupac);
		add(lblDostavljac);
		add(txtDostavljac);
		add(lblCena);
		add(txtCena);
		add(lblStatus);
		add(cbStatus);
		
		add(new JLabel());
		add(btnOk, "split 2");
		add(btnCancel);
		
	}
	private void initActions() {
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(validacija() == true) {
					String jelo = txtJelo.getText().trim();
					String pice = txtPice.getText().trim();
					String datum = txtDatum.getText().trim();
					String kupac = txtKupac.getText().trim();
					String dostavljac = txtDostavljac.getText().trim();
					double cena = Double.parseDouble(txtCena.getText().trim());
					String status = cbStatus.getSelectedItem().toString();
					if(narudzbina != null) {
						Narudzbina narudzbine = (Narudzbina) narudzbina;
						narudzbine.setJelo(jelo);
						narudzbine.setPice(pice);
						narudzbine.setVremeiDatum(datum);
						narudzbine.setKupac(kupac);
						narudzbine.setDostavljac(dostavljac);
						narudzbine.setCenaPorudzbine(cena);
						narudzbine.setStatus(status);
						
					}else {
						Narudzbina narudzbine = new Narudzbina(jelo, pice, datum, kupac, dostavljac, cena, status);
						restoran.getNarudzbine().add(narudzbine);
					}
				
			}
			
				restoran.snimiNarudzbine();
				NarudzbinaFormaKupacIzmena.this.dispose();
				NarudzbinaFormaKupacIzmena.this.setVisible(false);
		}
	});
		btnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				NarudzbinaFormaKupacIzmena.this.dispose();
				NarudzbinaFormaKupacIzmena.this.setVisible(false);
			}
		});
	}
		private void enablePolja(boolean enable) {
			txtJelo.setEnabled(!enable);
			txtPice.setEnabled(!enable);
			txtDatum.setEnabled(!enable);
			txtKupac.setEnabled(!enable);
			txtDostavljac.setEnabled(!enable);
			txtCena.setEnabled(!enable);
		}
		
		
		
		private boolean validacija() {
			boolean ok = true;
			String poruka = "Molimo popravite sledece greske u unosu:\n";
			
			
			if(ok == false) {
				JOptionPane.showMessageDialog(null, poruka, "Neispravni podaci", JOptionPane.WARNING_MESSAGE);
			}
			return ok;
		}
	
}
