package restoran;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import artikli.Artikli;
import artikli.Hrana;
import artikli.Narudzbina;
import artikli.Pice;
import artikli.RestoranLista;
import korisnici.Administrator;
import korisnici.Dostavljac;
import korisnici.Korisnici;
import korisnici.Kupac;
import korisnici.Pol;

public class Restoran {
	private ArrayList<Kupac> kupac;
	private ArrayList<Dostavljac> dostavljac;
	private ArrayList<Administrator> administrator;
	private ArrayList<RestoranLista> restoranLista;
	private ArrayList<Artikli> artikli;
	private ArrayList<Korisnici> korisnici;
	private ArrayList<Narudzbina> narudzbina;
	
	
	public Restoran() {
		this.kupac = new ArrayList<Kupac>();
		this.dostavljac = new ArrayList<Dostavljac>();
		this.administrator = new ArrayList<Administrator>();
		this.restoranLista = new ArrayList<RestoranLista>();
		this.artikli = new ArrayList<Artikli>();
		this.korisnici = new ArrayList<Korisnici>();
		this.narudzbina = new ArrayList<Narudzbina>();
	}
//-----------------------------------------Korisnici--------------------------------------------------
	public ArrayList<Korisnici> getKorisnici(){
		return korisnici;
	}
	public void dodajKorisnika(Korisnici korisnik) {
		this.korisnici.add(korisnik);
	}
	public void obrisiKorisnika(Korisnici korisnik) {
		this.korisnici.remove(korisnik);
	}
	public Korisnici nadjiKorisnike(String korisnickoIme) {
		for(Korisnici korisnik : korisnici) {
			if(korisnik.getKorIme().equals(korisnickoIme)) {
				return korisnik;
			}
		}
		return null;
	}
	public Korisnici login(String korisnickoIme, String sifra) {
		for(Korisnici korisnik: korisnici) {
			if(korisnik.getKorIme().equals(korisnickoIme) && korisnik.getLozinka().equals(sifra)){
				return korisnik;	
			}
				
		}
		return null;
	}
//----------------------------------------Kupac----------------------------------------------------------
	public ArrayList<Kupac> getKupac() {
		return kupac;
	}
	public void dodajKupca(Kupac kupci) {
		this.kupac.add(kupci);
	}
	public void obrisiKupce(Kupac kupci) {
		this.kupac.remove(kupci);
	}
	
	public Kupac nadjiKupce(String korisnickoIme) {
		for(Kupac kupci : kupac) {
			if(kupci.getKorIme().equals(korisnickoIme)) {
				return kupci;
			}
		}
		return null;
	}	
//----------------------------------------Dostavljac------------------------------------------------------
	public ArrayList<Dostavljac> getDostavljac() {
		return dostavljac;
	}
	public void dodajDostavljac(Dostavljac dostavljaci) {
		this.dostavljac.add(dostavljaci);
	}
	public void obrisiDostavljace(Dostavljac dostavljaci) {
		this.dostavljac.remove(dostavljaci);
	}
	
	public Dostavljac nadjiDostavljaca(String korisnickoIme) {
		for(Dostavljac dostavljaci : dostavljac) {
			if(dostavljaci.getKorIme().equals(korisnickoIme)) {
				return dostavljaci;
			}
		}
		return null;
	}
//-----------------------------------Administrator--------------------------------------------------------	
	public ArrayList<Administrator> getAdministraotor() {
		return administrator;
	}
	public void dodajAdministrator(Administrator administratori) {
		this.administrator.add(administratori);
	}
	
	public Administrator nadjiAdministratora(String korisnickoIme) {
		for(Administrator administratori : administrator) {
			if(administratori.getKorIme().equals(korisnickoIme)) {
				return administratori;
			}
		}
		return null;
	}
//------------------------------Ucitavanje Korisnika--------------------------------------------------------
	public void ucitajKorisnike() {
		try {
			File zaposleniFile = new File("src\\fajlovi\\korisnici.txt");
			BufferedReader br = new BufferedReader(new FileReader(zaposleniFile));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] split = line.split("\\|");
				String uloga = split[0];
				String ime = split[1];
				String prezime = split[2];
				int polInt = Integer.parseInt(split[3]);
				Pol pol = Pol.fromInt(polInt);
				String korisnickoIme = split[4];
				String sifra = split[5];
				Korisnici korisnik = new Korisnici(uloga, ime, prezime, pol, korisnickoIme, sifra) {};
				korisnici.add(korisnik);
				if (uloga.equals("Administrator")) {
					String jmbg = split[6];
					String plata = split[7];
					Administrator administratori = new Administrator(uloga, ime, prezime, pol, korisnickoIme, sifra, jmbg, plata);
					administrator.add(administratori);
				} else if (uloga.equals("Dostavljac")) {
					String jmbg = split[6];
					double plata = Double.parseDouble(split[7]);
					String registracijaVozila = split[8];
					String vozilo = split[9];
					Dostavljac dostavljaci = new Dostavljac(uloga, ime, prezime, pol, korisnickoIme, sifra, jmbg, plata, registracijaVozila, vozilo);
					dostavljac.add(dostavljaci);
				} else if (uloga.equals("Kupac")) {
					String brojTelefona = split[6];
					String adresa = split[7];
					Kupac kupci = new Kupac(uloga, ime, prezime, pol, korisnickoIme, sifra, brojTelefona, adresa);
					kupac.add(kupci);
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//-----------------------------------Restoran-------------------------------------------------------
	public ArrayList<RestoranLista> getRestoranLista() {
		return restoranLista;
	}
	public void dodajRestoranLista(RestoranLista restoranListe) {
		this.restoranLista.add(restoranListe);
	}
	public void obrisiRestorane(RestoranLista restoranListe) {
		this.restoranLista.remove(restoranListe);
	}
	public RestoranLista nadjiRestoran(String naziv) {
		for(RestoranLista restoranListe : restoranLista) {
			if(restoranListe.getRestoran().equals(naziv)) {
				return restoranListe;
			}
		}
		return null;
	}
	public void ucitajRestorane() {
		try {
		File zaposleniFile = new File("src\\fajlovi\\restoran.txt");
		BufferedReader br = new BufferedReader(new FileReader(zaposleniFile));
		String line = null;
		while ((line = br.readLine()) != null) {
			String[] split = line.split("\\|");
			String naziv = split[0];
			String adresa = split[1];
			String vrstaRestorana = split[2];
			RestoranLista restoranListe = new RestoranLista(naziv, adresa, vrstaRestorana);
			restoranLista.add(restoranListe);	
		}
		br.close();	
	}catch(Exception e) {
		e.printStackTrace();
	}
	}
	public void snimiRestorane() {
		try {
			File restoranFile = new File("src/fajlovi/restoran.txt");
			// Sve metode za snimanje rade tako sto se cela lista odredjenih entiteta snima u fajl odjednom.
			// Ovaj string predstavlja sadrzaj koji ce se upisati u datoteku
			String content = "";
			// Za svaki entitet u list sastavimo tekst koji predstavlja jednu liniju u datoteci i dodamo ga na sadrzaj
			for (RestoranLista restoranListe : restoranLista) {
				content += 
							restoranListe.getRestoran() + "|" +
							restoranListe.getAdresa() + "|" +
							restoranListe.getVrstaKuhinje() + "\n";
							
	
			}
			
			// Na kraju upisemo formirani sadrzaj u datoteku
			BufferedWriter writer = new BufferedWriter(new FileWriter(restoranFile));
			writer.write(content);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
//--------------------------------------Artikli-------------------------------------------------------
	public ArrayList<Artikli> getArtikli() {
		return artikli;
	}
	public void dodajArtikle(Artikli artikl) {
		this.artikli.add(artikl);
	}
	public void obrisiArtikle(Artikli artikl) {
		this.artikli.remove(artikl);
	}
	public Artikli nadjiArtikle(String naziv) {
		for(Artikli artikli : artikli) {
			if(artikli.getJelo().equals(naziv)) {
				return artikli;
			}
		}
		return null;
	}
	
//------------------------------------Ucitaj Artikle------------------------------------------------------
	public void ucitajArtikle() {
		try {
			File artikliFile = new File("src\\fajlovi\\artikal.txt");
			BufferedReader br = new BufferedReader(new FileReader(artikliFile));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] split = line.split("\\|");
				String JeloPice = split[0];
				String restoran = split[1];
				String nazivProizvoda = split[2];
				String cenaString = split[3];
				double cena = Double.parseDouble(cenaString);
				String vrsta = split[4];
				String kolicinaString = split[5];
				double kolicina = Double.parseDouble(kolicinaString);
				Artikli artikl = new Artikli(JeloPice, restoran, nazivProizvoda, cena, vrsta, kolicina);
				artikli.add(artikl);
			}
			br.close();
	}catch(Exception e) {
		e.printStackTrace();
	}
	}	
//-------------------------------------Narudzbine-----------------------------------------------------

	public ArrayList<Narudzbina> getNarudzbine() {
		return narudzbina;
	}
	public void dodajNarudzbine(Narudzbina narudzbine) {
		this.narudzbina.add(narudzbine);
	}
	public void obrisiNarudzbine(Narudzbina narudzbine) {
		this.narudzbina.remove(narudzbine);
	}
	public Narudzbina nadjiNarudzbineDostavljac(String dostavljac) {
		for(Narudzbina narudzbine : narudzbina) {
			if(narudzbine.getDostavljac().equals(dostavljac)) {
				return narudzbine;
			}
		}
		return null;
	}
//-------------------------------------Snimi Korisnike---------------------------------------------------------
	public void snimiKorisnike() {
		try {
			File korisniciFile = new File("src/fajlovi/korisnici.txt");
			// Sve metode za snimanje rade tako sto se cela lista odredjenih entiteta snima u fajl odjednom.
			// Ovaj string predstavlja sadrzaj koji ce se upisati u datoteku
			String content = "";
			// Za svaki entitet u list sastavimo tekst koji predstavlja jednu liniju u datoteci i dodamo ga na sadrzaj
			for (Kupac kupci : kupac) {
				content += "Kupac|" +
							kupci.getIme() + "|" +
							kupci.getPrezime() + "|" +
							Pol.toInt(kupci.getPol()) + "|" +
							kupci.getKorIme() + "|" +
							kupci.getLozinka() + "|" +
							kupci.getAdresa() + "|" +
							kupci.getBrTelefona() + "\n";
			}
			for (Dostavljac dostavljaci : dostavljac) {
				content += "Dostavljac|" +

							dostavljaci.getIme() + "|" +
							dostavljaci.getPrezime() + "|" +
							Pol.toInt(dostavljaci.getPol()) + "|" +
							dostavljaci.getKorIme() + "|" +
							dostavljaci.getLozinka() + "|" +
							dostavljaci.getJmbg() + "|" +
							dostavljaci.getPlata() + "|" +
							dostavljaci.getTablice() + "|" +
							dostavljaci.getVozilo() + "\n";
			}
			for (Administrator administratori : administrator) {
				content += "Administrator|" +

							administratori.getIme() + "|" +
							administratori.getPrezime() + "|" +
							Pol.toInt(administratori.getPol()) + "|" +
							administratori.getKorIme() + "|" +
							administratori.getLozinka() + "|" +
							administratori.getJmbg() + "|" +
							administratori.getPlata() + "\n";
							
			}
			
			BufferedWriter writer = new BufferedWriter(new FileWriter(korisniciFile));
			writer.write(content);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
//---------------------------------------Ucitaj Narudzbine----------------------------------------------------------
	public void ucitajNarudzbine() {
		try {
			File narudzbineFile = new File("src\\fajlovi\\narudzbine.txt");
			BufferedReader br = new BufferedReader(new FileReader(narudzbineFile));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] split = line.split("\\|");
				String Jelo = split[0];
				String pice = split[1];
				String datumiVreme = split[2];
				String kupac = split[3];
				String dostavljac = split[4];
				String cenaString = split[5];
				double cena = Double.parseDouble(cenaString);
				String status = split[6];
				Narudzbina narudzbine = new Narudzbina(Jelo, pice, datumiVreme , kupac, dostavljac, cena, status);
				narudzbina.add(narudzbine);
			}
			br.close();
	}catch(Exception e) {
		e.printStackTrace();
	}
	}	
//---------------------------------------Snimi Artikle----------------------------------------------------------
	public void snimiArtikle() {
		try {
			File artikliFile = new File("src/fajlovi/artikal.txt");
			// Sve metode za snimanje rade tako sto se cela lista odredjenih entiteta snima u fajl odjednom.
			// Ovaj string predstavlja sadrzaj koji ce se upisati u datoteku
			String content = "";
			// Za svaki entitet u list sastavimo tekst koji predstavlja jednu liniju u datoteci i dodamo ga na sadrzaj
			for (Artikli artikal : artikli) {
				content += 
							artikal.getJeloPice() + "|" +
							artikal.getRestoran() + "|" +
							artikal.getJelo() + "|" +
							artikal.getCena() + "|" +
							artikal.getVrsta() + "|" +
							artikal.getKolicina() + "\n";
	
			}
			
			// Na kraju upisemo formirani sadrzaj u datoteku
			BufferedWriter writer = new BufferedWriter(new FileWriter(artikliFile));
			writer.write(content);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
//--------------------------------------Snimi Narudzbine---------------------------------------------------------
	public void snimiNarudzbine() {
		try {
			File narudzbineFile = new File("src/fajlovi/narudzbine.txt");
			// Sve metode za snimanje rade tako sto se cela lista odredjenih entiteta snima u fajl odjednom.
			// Ovaj string predstavlja sadrzaj koji ce se upisati u datoteku
			String content = "";
			// Za svaki entitet u list sastavimo tekst koji predstavlja jednu liniju u datoteci i dodamo ga na sadrzaj
			for (Narudzbina narudzbine : narudzbina) {
				content += 
								narudzbine.getJelo() + "|" +
								narudzbine.getPice() + "|" +
								narudzbine.getVremeiDatum() + "|" +
								narudzbine.getKupac()+ "|" +
								narudzbine.getDostavljac() + "|" +
								narudzbine.getCenaPorudzbine() + "|" +
								narudzbine.getStatus() + "\n";
	
			}
			
			// Na kraju upisemo formirani sadrzaj u datoteku
			BufferedWriter writer = new BufferedWriter(new FileWriter(narudzbineFile));
			writer.write(content);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

