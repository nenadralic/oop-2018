package artikli;

public class Pice {
	private String pice;
	
	public Pice() {
		this.pice = "";
	}

	public Pice(String pice) {
		super();
		this.pice = pice;
	}

	public String getPice() {
		return pice;
	}

	public void setPice(String pice) {
		this.pice = pice;
	}

	@Override
	public String toString() {
		return "Pice [pice=" + pice + "]";
	}
	
	
}
