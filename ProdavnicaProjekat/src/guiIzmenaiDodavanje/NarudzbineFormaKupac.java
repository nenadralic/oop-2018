package guiIzmenaiDodavanje;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import artikli.Artikli;
import artikli.Narudzbina;
import artikli.RestoranLista;
import korisnici.Korisnici;
import korisnici.Pol;
import net.miginfocom.swing.MigLayout;
import restoran.Restoran;

public class NarudzbineFormaKupac extends JFrame {
	private JLabel lblJelo = new JLabel("Jelo");
	private JTextField txtJelo = new JTextField(20);
	private JLabel lblPice = new JLabel("Pice");
	private JTextField txtPice = new JTextField(20);
	private JLabel lblCenaPica = new JLabel("Cena pica");
	private JTextField txtCenaPica = new JTextField(20);
	private JLabel lblCenaJela = new JLabel("Cena jela");
	private JTextField txtCenaJela = new JTextField(20);
	private JButton btnJelo = new JButton("Dodaj u korpu");
	private JLabel lbl = new JLabel("");
	private JLabel lbljedan = new JLabel("");
	private JLabel lbldva = new JLabel("");
	private JLabel lbltri = new JLabel("");
	private JLabel lblcetiri = new JLabel("");
	private JTextField txtRestoranJelo = new JTextField(20);
	private JLabel lblpet = new JLabel("");
	private JLabel lblsest = new JLabel("");
	private JTextField txtRestoranPice = new JTextField(20);
	private JButton btnUkloniJelo = new JButton("Ukloni jelo");
	private JButton btnUkloniPice = new JButton("Ukloni pice");
	
	
	
	
	private JButton btnOk = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	
	private Restoran restoran;
	private Korisnici prijavljeniKorisnik;
	private Artikli artikal;
	private Narudzbina narudzbina;
	private DefaultTableModel tableModel;
	private JTable artikliTabela;
	
	public NarudzbineFormaKupac(Restoran restoran, Korisnici prijavljeniKorisnik) {
		this.restoran = restoran;
		this.prijavljeniKorisnik = prijavljeniKorisnik;
		this.artikal = artikal;
		this.narudzbina = narudzbina;
		if(this.prijavljeniKorisnik == null) {
			setTitle("Dodavanje artikala");
		}else {
			setTitle("Izmena podataka - " + this.prijavljeniKorisnik.getKorIme());
		}
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		initGUI();
		initActions();
		setResizable(false);
		pack();

	}
	private void initGUI() {
		MigLayout layout = new MigLayout("wrap 3");
		setLayout(layout);
		
		
	
		enablePolja(true);									
		
		add(lblJelo);
		add(txtJelo);
		add(btnJelo);
		add(lblPice);
		add(txtPice);
		add(lbl);
		add(lblCenaJela);
		add(txtCenaJela);
		add(lbljedan);
		add(lblCenaPica);
		add(txtCenaPica);
		add(lbldva);
		add(lbltri);
		add(lblcetiri);
		add(txtRestoranJelo).setVisible(false);
		add(lblpet);
		add(lblsest);
		add(txtRestoranPice).setVisible(false);
				
		String[] zaglavlje = new String[] {"Vrsta" ,"Restoran", "Naziv", "Cena", "Opis", "Kolicina"};
		Object[][] podaci = new Object[this.restoran.getArtikli().size()][zaglavlje.length];
		
		for(int i=0; i<this.restoran.getArtikli().size(); i++) {
			Artikli artikl = this.restoran.getArtikli().get(i);
			podaci[i][0] = artikl.getJeloPice();
			podaci[i][1] = artikl.getRestoran();
			podaci[i][2] = artikl.getJelo();
			podaci[i][3] = artikl.getCena();
			podaci[i][4] = artikl.getVrsta();
			podaci[i][5] = artikl.getKolicina();
			
		}
		tableModel = new DefaultTableModel(podaci, zaglavlje);
		artikliTabela = new JTable(tableModel);
		artikliTabela = new JTable(tableModel);
		artikliTabela.setRowSelectionAllowed(true);
		artikliTabela.setColumnSelectionAllowed(false);
		artikliTabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		artikliTabela.setDefaultEditor(Object.class, null);
		
		JScrollPane scrollPane = new JScrollPane(artikliTabela);
		add(scrollPane, BorderLayout.CENTER);
		
		add(new JLabel());
		add(btnUkloniJelo, "split 2");
		add(btnUkloniPice);
		
		
		add(new JLabel());
		add(btnOk, "split 2");
		add(btnCancel);
		
	}
	private void initActions() {
		btnJelo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int red = artikliTabela.getSelectedRow();
				DefaultTableModel model = (DefaultTableModel)artikliTabela.getModel();
				String jelo = model.getValueAt(red, 2).toString();
				String vrsta = model.getValueAt(red, 0).toString();
				String cena = model.getValueAt(red, 3).toString();
				String restoran = model.getValueAt(red, 1).toString();
				if(vrsta.equals("Jelo")) {
					txtJelo.setText(jelo);
					txtCenaJela.setText(cena);
					txtRestoranJelo.setText(restoran);
				}else {
					txtPice.setText(jelo);
					txtCenaPica.setText(cena);
					txtRestoranPice.setText(restoran);
				}
				

			}
		});
		btnUkloniJelo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
					txtJelo.setText("");
					txtCenaJela.setText("");
					txtRestoranJelo.setText("");
			}
		});
		btnUkloniPice.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
					txtPice.setText("");
					txtCenaPica.setText("");
					txtRestoranPice.setText("");
			}
		});
		
			
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(validacija() == true) {
					String jelo = txtJelo.getText().trim();
					String pice = txtPice.getText().trim();
					Date date = new Date();
					String datum = date.toString();
					String kupac = prijavljeniKorisnik.getKorIme();
					String dostavljac = "";
					String status = "Porucena";
					double cena = Double.parseDouble(txtCenaJela.getText().trim()) + Double.parseDouble(txtCenaPica.getText().trim()) + 200;
					if(narudzbina != null) {
							Narudzbina narudzbine = (Narudzbina) narudzbina;
							narudzbine.setJelo(jelo);
							narudzbine.setPice(pice);
							narudzbine.setVremeiDatum(datum);
							narudzbine.setKupac(kupac);
							narudzbine.setDostavljac(dostavljac);
							narudzbine.setCenaPorudzbine(cena);
							narudzbine.setStatus(status);
							
						}else {
							Narudzbina narudzbine = new Narudzbina(jelo, pice, datum, kupac, dostavljac, cena, status);
							restoran.getNarudzbine().add(narudzbine);
						}
					
				}
				
					restoran.snimiNarudzbine();
					NarudzbineFormaKupac.this.dispose();
					NarudzbineFormaKupac.this.setVisible(false);
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				NarudzbineFormaKupac.this.dispose();
				NarudzbineFormaKupac.this.setVisible(false);
			}
		});
	}

	private void enablePolja(boolean enable) {
		txtCenaPica.setEnabled(!enable);
		txtCenaJela.setEnabled(!enable);
		txtJelo.setEnabled(!enable);
		txtPice.setEnabled(!enable);
		txtRestoranJelo.setEnabled(!enable);
		txtRestoranPice.setEnabled(!enable);
	}
	
	
	
	private boolean validacija() {
		boolean ok = true;
		String poruka = "Molimo popravite sledece greske u unosu:\n";
		if(txtRestoranJelo.getText().trim().equals(txtRestoranPice.getText().trim())) {
			ok = true;
		}else {
			poruka += "- Narudzbina mora biti iz istog restorana!\n";
			ok = false;
		}
		
		if(ok == false) {
			JOptionPane.showMessageDialog(null, poruka, "Neispravni podaci", JOptionPane.WARNING_MESSAGE);
		}
		return ok;
	}
}
