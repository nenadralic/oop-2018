package guiIzmenaiDodavanje;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import artikli.Artikli;
import artikli.RestoranLista;
import korisnici.Dostavljac;
import korisnici.Korisnici;
import korisnici.Kupac;
import korisnici.Pol;
import net.miginfocom.swing.MigLayout;
import restoran.Restoran;

public class ArtikliForma extends JFrame {
	private JLabel lblVrsta = new JLabel("Vrsta");
	private JComboBox<String> cbArtikli = new JComboBox<String>();
	private JLabel lblRestoran = new JLabel("Restoran");
	private JComboBox<String> cbRestoran = new JComboBox<String>();
	private JLabel lblNaziv = new JLabel("Naziv");
	private JTextField txtNaziv = new JTextField(20);
	private JLabel lblCena = new JLabel("Cena");
	private JTextField txtCena = new JTextField(20);
	private JLabel lblOpis = new JLabel("Opis");
	private JTextField txtOpis = new JTextField(20);
	private JLabel lblKolicina = new JLabel("Kolicina");
	private JTextField txtKolicina = new JTextField(20);
	
	private JButton btnOk = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	
	private Restoran restoran;
	private Korisnici korisnik;
	private Artikli artikal;
	
	public ArtikliForma(Restoran restoran, Artikli artikal) {
		this.restoran = restoran;
		this.korisnik = korisnik;
		this.artikal = artikal;
		if(this.korisnik == null) {
			setTitle("Dodavanje artikala");
		}else {
			setTitle("Izmena podataka - " + this.korisnik.getKorIme());
		}
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		initGUI();
		initActions();
		setResizable(false);
		pack();
	}
	
	private void initGUI() {
		MigLayout layout = new MigLayout("wrap 2");
		setLayout(layout);
		
		if(this.korisnik != null) {
			popuniPolja();
		}
		cbArtikli.addItem("Jelo");
		cbArtikli.addItem("Pice");
		for (RestoranLista restorani : this.restoran.getRestoranLista()) {
			cbRestoran.addItem(restorani.getRestoran());
		}
		if(this.artikal != null) {
			txtNaziv.setText(this.artikal.getJelo());
			txtCena.setText(String.valueOf(this.artikal.getCena()));
			txtOpis.setText(this.artikal.getVrsta());
			txtKolicina.setText(String.valueOf(this.artikal.getKolicina()));
		}
		
		add(lblVrsta);
		add(cbArtikli);
		add(lblRestoran);
		add(cbRestoran);
		add(lblNaziv);
		add(txtNaziv);
		add(lblCena);
		add(txtCena);
		add(lblOpis);
		add(txtOpis);
		add(lblKolicina);
		add(txtKolicina);
		
		add(new JLabel());
		add(btnOk, "split 2");
		add(btnCancel);
	}
	private void initActions() {
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(validacija() == true) {
					String vrsta = cbArtikli.getSelectedItem().toString();
					String restoranNaziv = cbRestoran.getSelectedItem().toString();
					String naziv = txtNaziv.getText().trim();
					double cena = Double.parseDouble(txtCena.getText().trim());
					String opis = txtOpis.getText().trim();
					double kolicina = Double.parseDouble(txtKolicina.getText().trim());
					if(artikal == null) {
						Artikli artikli = new Artikli(vrsta, restoranNaziv, naziv, cena, opis, kolicina);
						restoran.getArtikli().add(artikli);
						}else {
							Artikli artikli = (Artikli) artikal;
							artikli.setJeloPice(vrsta);
							artikli.setRestoran(restoranNaziv);
							artikli.setJelo(naziv);
							artikli.setCena(cena);
							artikli.setVrsta(opis);
							artikli.setKolicina(kolicina);
							
						}
					
					}
					restoran.snimiArtikle();
					ArtikliForma.this.dispose();
					ArtikliForma.this.setVisible(false);
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ArtikliForma.this.dispose();
				ArtikliForma.this.setVisible(false);
			}
		});	
	}

	
	
	private void popuniPolja() {
		cbArtikli.setSelectedItem(this.artikal.getJeloPice());
		cbRestoran.setSelectedItem(this.artikal.getRestoran());
		txtNaziv.setText(this.artikal.getJelo());
		txtCena.setText(String.valueOf(this.artikal.getCena()));
		txtOpis.setText(this.artikal.getVrsta());
		txtKolicina.setText(String.valueOf(this.artikal.getKolicina()));
		
	}
	
	private boolean validacija() {
		boolean ok = true;
		String poruka = "Molimo popravite sledece greske u unosu:\n";
		
		if(txtNaziv.getText().trim().equals("")) {
			poruka += "- Morate uneti naziv\n";
			ok = false;
		}
		if(txtOpis.getText().trim().equals("")) {
			poruka += "- Morate uneti opis\n";
			ok = false;
		}
		if(txtCena.getText().trim().equals("")) {
			poruka += "- Morate uneti platu\n";
			ok = false;
		}
		try {
			Double.parseDouble(txtCena.getText().trim());
		}catch (NumberFormatException e) {
			poruka += "- Cena mora biti broj\n";
			ok = false;
		}
		if(txtKolicina.getText().trim().equals("")) {
			poruka += "- Morate uneti kolicinu\n";
			ok = false;
		}
		try {
			Double.parseDouble(txtKolicina.getText().trim());
		}catch (NumberFormatException e) {
			poruka += "- Kolicina mora biti broj\n";
			ok = false;
		}
		
		if(ok == false) {
			JOptionPane.showMessageDialog(null, poruka, "Neispravni podaci", JOptionPane.WARNING_MESSAGE);
		}
		return ok;
	}
	
}
