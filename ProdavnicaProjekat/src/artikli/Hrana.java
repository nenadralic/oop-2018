package artikli;

public class Hrana {
	private String jelo;
	
	public Hrana() {
		this.jelo = "";
	}

	public Hrana(String jelo) {
		super();
		this.jelo = jelo;
	}

	public String getJelo() {
		return jelo;
	}

	public void setJelo(String jelo) {
		this.jelo = jelo;
	}

	@Override
	public String toString() {
		return "Hrana [jelo=" + jelo + "]";
	}
	
	
}
