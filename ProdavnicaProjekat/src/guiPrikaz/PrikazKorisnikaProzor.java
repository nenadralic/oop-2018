package guiPrikaz;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import guiIzmenaiDodavanje.KorisniciForma;
import korisnici.Administrator;
import korisnici.Dostavljac;
import korisnici.Korisnici;
import korisnici.Kupac;
import restoran.Restoran;

public class PrikazKorisnikaProzor extends JFrame {
	private JToolBar mainToolbar = new JToolBar();
	private JButton btnAdd = new JButton();
	private JButton btnEdit = new JButton();
	private JButton btnDelete = new JButton();
	
	private DefaultTableModel tableModel;
	private JTable korisniciTabela;
	
	private Restoran restoran;
	
	public PrikazKorisnikaProzor(Restoran restoran) {
		this.restoran = restoran;
		setTitle("Korisnici");
		setSize(300, 300);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		initGUI();
		initActions();
	}
	private void initGUI() {
		ImageIcon addIcon = new ImageIcon(getClass().getResource("/slike/add.png"));
		btnAdd.setIcon(addIcon);
		mainToolbar.add(btnAdd);
		ImageIcon editIcon = new ImageIcon(getClass().getResource("/slike/edit.png"));
		btnEdit.setIcon(editIcon);
		mainToolbar.add(btnEdit);
		ImageIcon deleteIcon = new ImageIcon(getClass().getResource("/slike/remove.png"));
		btnDelete.setIcon(deleteIcon);
		mainToolbar.add(btnDelete);
		add(mainToolbar, BorderLayout.NORTH);
		
		int brojKorisnika = restoran.getKupac().size() + restoran.getAdministraotor().size() + restoran.getDostavljac().size();
		String[] zaglavlje = new String[] {"Uloga" ,"Ime", "Prezime", "Pol", "Korisnicko ime", "Sifra", "Adresa" , "Broj Telefona", "Jmbg", "Plata", "Registracija", "Tip vozila"};
		Object[][] podaci = new Object[brojKorisnika][zaglavlje.length];
		
		for(int i=0; i<this.restoran.getKupac().size(); i++) {
			Kupac kupci = this.restoran.getKupac().get(i);
			podaci[i][0] = "Kupac";
			podaci[i][1] = kupci.getIme();
			podaci[i][2] = kupci.getPrezime();
			podaci[i][3] = kupci.getPol();
			podaci[i][4] = kupci.getKorIme();
			podaci[i][5] = kupci.getLozinka();
			podaci[i][6] = kupci.getAdresa();
			podaci[i][7] = kupci.getBrTelefona();
			podaci[i][8] = "/";
			podaci[i][9] = "/";
			podaci[i][10] = "/";
			podaci[i][11] = "/";
			
		}
		for(int i=0; i<this.restoran.getDostavljac().size(); i++) {
			Dostavljac dostavljaci = this.restoran.getDostavljac().get(i);
			int redA = restoran.getKupac().size() + i;
			podaci[redA][0] = "Dostavljac";
			podaci[redA][1] = dostavljaci.getIme();
			podaci[redA][2] = dostavljaci.getPrezime();
			podaci[redA][3] = dostavljaci.getPol();
			podaci[redA][4] = dostavljaci.getKorIme();
			podaci[redA][5] = dostavljaci.getLozinka();
			podaci[redA][6] = "/";
			podaci[redA][7] = "/";
			podaci[redA][8] = dostavljaci.getJmbg();
			podaci[redA][9] = dostavljaci.getPlata();
			podaci[redA][10] = dostavljaci.getTablice();
			podaci[redA][11] = dostavljaci.getVozilo();
			
		}
		for(int i=0; i<this.restoran.getAdministraotor().size(); i++) {
			Administrator administratori = this.restoran.getAdministraotor().get(i);
			int redB = restoran.getDostavljac().size() + restoran.getKupac().size()+i;
			podaci[redB][0] = "Administrator";
			podaci[redB][1] = administratori.getIme();
			podaci[redB][2] = administratori.getPrezime();
			podaci[redB][3] = administratori.getPol();
			podaci[redB][4] = administratori.getKorIme();
			podaci[redB][5] = administratori.getLozinka();
			podaci[redB][6] = "/";
			podaci[redB][7] = "/";
			podaci[redB][8] = administratori.getJmbg();
			podaci[redB][9] = administratori.getPlata();
			podaci[redB][10] = "/";
			podaci[redB][11] = "/";
			
		}
		
		tableModel = new DefaultTableModel(podaci, zaglavlje);
		korisniciTabela = new JTable(tableModel);
		korisniciTabela = new JTable(tableModel);
		korisniciTabela.setRowSelectionAllowed(true);
		korisniciTabela.setColumnSelectionAllowed(false);
		korisniciTabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		korisniciTabela.setDefaultEditor(Object.class, null);
		
		JScrollPane scrollPane = new JScrollPane(korisniciTabela);
		add(scrollPane, BorderLayout.CENTER);
	}
	private void initActions() {
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				KorisniciForma zf = new KorisniciForma(restoran, null);
				zf.setVisible(true);
			}
		});
		btnEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int red = korisniciTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate odabrati red u tabeli.", "Greska", JOptionPane.WARNING_MESSAGE);
				}else {
					DefaultTableModel model = (DefaultTableModel)korisniciTabela.getModel();
					String korisnickoIme = model.getValueAt(red, 4).toString();
					Korisnici korisnik = restoran.nadjiKorisnike(korisnickoIme);
					if(korisnik != null) {
						KorisniciForma zf = new KorisniciForma(restoran, korisnik);
						zf.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "Nije moguce pronaci odabranog korisnika!", "Greska", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btnDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int red = korisniciTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate odabrati red u tabeli.", "Greska", JOptionPane.WARNING_MESSAGE);
				}else {
					DefaultTableModel model = (DefaultTableModel)korisniciTabela.getModel();
					String korisnickoIme = model.getValueAt(red, 4).toString();
					Korisnici korisnik = restoran.nadjiKorisnike(korisnickoIme);
					if(korisnik != null) {
						int izbor = JOptionPane.showConfirmDialog(null, "Da li ste sigurni da zelite da obrisete korisnika?", korisnik.getKorIme() + " - Potvrda brisanja", JOptionPane.YES_NO_OPTION);
						if(izbor == JOptionPane.YES_OPTION) {
							restoran.getKorisnici().remove(korisnik);
							model.removeRow(red);
							restoran.snimiKorisnike();
						}
					}else {
						JOptionPane.showMessageDialog(null, "Nije moguce pronaci odabranog korisnika!", "Greska", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
	}
}
