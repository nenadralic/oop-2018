package guiIzmenaiDodavanje;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


import korisnici.Dostavljac;
import korisnici.Korisnici;
import korisnici.Kupac;
import korisnici.Pol;
import net.miginfocom.swing.MigLayout;
import restoran.Restoran;

public class KorisniciForma extends JFrame {
	private JLabel lblIme = new JLabel("Ime");
	private JTextField txtIme = new JTextField(20);
	private JLabel lblPrezime = new JLabel("Prezime");
	private JTextField txtPrezime = new JTextField(20);
	private JLabel lblPol = new JLabel("Pol");
	private JComboBox<Pol> cbPol = new JComboBox<Pol>(Pol.values());
	private JLabel lblKorisnickoIme = new JLabel("Korisnicko ime");
	private JTextField txtKorisnickoIme = new JTextField(20);
	private JLabel lblSifra = new JLabel("Sifra");
	private JPasswordField pfSifra = new JPasswordField(20);
	private JLabel lbltipKor = new JLabel("Tip korisnika: ");
	private JRadioButton rbtnKupac = new JRadioButton("Kupac");
	private JRadioButton rbtnDostavljac = new JRadioButton("Dostavljac");
	private JLabel lbladresa = new JLabel("Adresa");
	private JTextField txtAdresa = new JTextField(20);
	private JLabel lblbrTelefona = new JLabel("Broj telefona");
	private JTextField txtbrTelefona = new JTextField(20);
	private JLabel lbljmbg = new JLabel("JMBG");
	private JTextField txtjmbg = new JTextField(20);
	private JLabel lblplata = new JLabel("Plata");
	private JTextField txtplata = new JTextField(20);
	private JLabel lblregOznaka = new JLabel("Registarska oznaka");
	private JTextField txtregOznaka = new JTextField(20);
	private JLabel lbltipVozila = new JLabel("Tip vozila");
	private JTextField txttipVozila= new JTextField(20);
	
	
	private JButton btnOk = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	
	private Restoran restoran;
	private Korisnici korisnik;
	
	public KorisniciForma(Restoran restoran, Korisnici korisnik) {
		this.restoran = restoran;
		this.korisnik = korisnik;
		if(this.korisnik == null) {
			setTitle("Dodavanje prodavca");
		}else {
			setTitle("Izmena podataka - " + this.korisnik.getKorIme());
		}
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		initGUI();
		initActions();
		setResizable(false);
		pack();
	}
	private void initGUI() {
		MigLayout layout = new MigLayout("wrap 2");
		setLayout(layout);
		
		if(this.korisnik != null) {
			popuniPolja();
		}else {
			rbtnKupac.setSelected(true);
			enableKupacPolja(true);
		}
		
		add(lblIme);
		add(txtIme);
		add(lblPrezime);
		add(txtPrezime);
		add(lblPol);
		add(cbPol);
		add(lblKorisnickoIme);
		add(txtKorisnickoIme);
		add(lblSifra);
		add(pfSifra);
		add(lbltipKor);
		ButtonGroup tipGroup = new ButtonGroup();
		tipGroup.add(rbtnKupac);
		tipGroup.add(rbtnDostavljac);
		add(rbtnKupac, "split 2");
		add(rbtnDostavljac);
		add(lbladresa);
		add(txtAdresa);
		add(lblbrTelefona);
		add(txtbrTelefona);
		add(lbljmbg);
		add(txtjmbg);
		add(lblplata);
		add(txtplata);
		add(lblregOznaka);
		add(txtregOznaka);
		add(lbltipVozila);
		add(txttipVozila);
		add(new JLabel());
		add(btnOk, "split 2");
		add(btnCancel);
	}
	private void initActions() {
		rbtnKupac.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				enableKupacPolja(rbtnKupac.isSelected());
			}
		});
		rbtnDostavljac.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				enableDostavljacPolja(rbtnDostavljac.isSelected());
			}
		});
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(validacija() == true) {
					String ime = txtIme.getText().trim();
					String prezime = txtPrezime.getText().trim();
					Pol pol = (Pol) cbPol.getSelectedItem();
					String korisnickoIme = txtKorisnickoIme.getText().trim();
					String sifra = new String(pfSifra.getPassword()).trim();
					if(rbtnKupac.isSelected()) {
						String adresa = txtAdresa.getText().trim();
						String brTelefona = txtbrTelefona.getText().trim();
						if(korisnik != null) {
							Kupac kupac = (Kupac) korisnik;
							kupac.setIme(ime);
							kupac.setPrezime(prezime);
							kupac.setPol(pol);
							kupac.setKorIme(korisnickoIme);
							kupac.setLozinka(sifra);
							kupac.setAdresa(adresa);
							kupac.setBrTelefona(brTelefona);
						}else {
							Kupac kupac = new Kupac("Kupac", ime, prezime, pol, korisnickoIme, sifra, adresa, brTelefona);
							restoran.getKupac().add(kupac);
						}
					}else {
						String jmbg = txtjmbg.getText().trim();
						double plata = Double.parseDouble(txtplata.getText().trim());
						String regOznaka = txtregOznaka.getText().trim();
						String tipVozila = txttipVozila.getText().trim();
						if(korisnik != null) {
							Dostavljac dostavljac = (Dostavljac) korisnik;
							dostavljac.setIme(ime);
							dostavljac.setPrezime(prezime);
							dostavljac.setPol(pol);
							dostavljac.setKorIme(korisnickoIme);
							dostavljac.setLozinka(sifra);
							dostavljac.setPlata(plata);
							dostavljac.setTablice(regOznaka);
							dostavljac.setVozilo(tipVozila);
						}else {
							Dostavljac dostavljac = new Dostavljac("Dostavljac", ime, prezime, pol, korisnickoIme, sifra, jmbg, plata, regOznaka, tipVozila);
							restoran.getDostavljac().add(dostavljac);
						}
					}
					restoran.snimiKorisnike();
					KorisniciForma.this.dispose();
					KorisniciForma.this.setVisible(false);
				}
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				KorisniciForma.this.dispose();
				KorisniciForma.this.setVisible(false);
			}
		});
	}
	private void enableKupacPolja(boolean enable) {
		txtAdresa.setEnabled(enable);
		txtbrTelefona.setEnabled(enable);
		txtjmbg.setEnabled(!enable);
		txtplata.setEnabled(!enable);
		txtregOznaka.setEnabled(!enable);
		txttipVozila.setEnabled(!enable);
		
	}
	
	private void enableDostavljacPolja(boolean enable) {
		txtAdresa.setEnabled(!enable);
		txtbrTelefona.setEnabled(!enable);
		txtjmbg.setEnabled(enable);
		txtplata.setEnabled(enable);
		txtregOznaka.setEnabled(enable);
		txttipVozila.setEnabled(enable);
		
	}
	private void popuniPolja() {
		txtIme.setText(this.korisnik.getIme());
		txtPrezime.setText(this.korisnik.getPrezime());
		cbPol.setSelectedItem(this.korisnik.getPol());
		txtKorisnickoIme.setText(this.korisnik.getIme());
		pfSifra.setText(this.korisnik.getLozinka());
		if(korisnik.getZanimanje() == "Kupac") {
			Kupac kupac = (Kupac) korisnik;
			enableKupacPolja(true);
			txtAdresa.setText(kupac.getAdresa());
			txtbrTelefona.setText(kupac.getBrTelefona());
		}else if(korisnik.getZanimanje() == "Dostavljac") {
			Dostavljac dostavljaci = (Dostavljac) korisnik;
			enableDostavljacPolja(true);
			txtjmbg.setText(String.valueOf(dostavljaci.getJmbg()));
			txtplata.setText(String.valueOf(dostavljaci.getPlata()));
			txtregOznaka.setText(dostavljaci.getTablice());
			txttipVozila.setText(dostavljaci.getVozilo());
			
		}
		// disable-ujemo radio buttone, jer ako radimo izmenu ne treba dozvoliti promenu tipa
		rbtnKupac.setEnabled(false);
		rbtnDostavljac.setEnabled(false);
	}
	
	private boolean validacija() {
		boolean ok = true;
		String poruka = "Molimo popravite sledece greske u unosu:\n";
		
		if(txtIme.getText().trim().equals("")) {
			poruka += "- Morate uneti ime\n";
			ok = false;
		}
		if(txtPrezime.getText().trim().equals("")) {
			poruka += "- Morate uneti prezime\n";
			ok = false;
		}
		
		if(txtKorisnickoIme.getText().trim().equals("")) {
			poruka += "- Morate uneti korisnicko ime\n";
			ok = false;
		}
		if(rbtnKupac.isSelected()) {
			if(txtAdresa.getText().trim().equals("")) {
				poruka += "- Morate uneti adresu\n";
				ok = false;
			}
			if(txtbrTelefona.getText().trim().equals("")) {
				poruka += "- Morate uneti broj telefona\n";
				ok = false;
			}
		}else {
			if(txtjmbg.getText().trim().equals("")) {
				poruka += "- Morate uneti JMBG\n";
				ok = false;
			}
			try {
				Integer.parseInt(txtjmbg.getText().trim());
			}catch (NumberFormatException e) {
				poruka += "- JMBG mora biti ceo broj\n";
				ok = false;
			}
			if(txtplata.getText().trim().equals("")) {
				poruka += "- Morate uneti platu\n";
				ok = false;
			}
			try {
				Double.parseDouble(txtplata.getText().trim());
			}catch (NumberFormatException e) {
				poruka += "- Plata mora biti broj\n";
				ok = false;
			}
			if(txtregOznaka.getText().trim().equals("")) {
				poruka += "- Morate uneti registarsku oznaku\n";
				ok = false;
			}
			if(txttipVozila.getText().trim().equals("")) {
				poruka += "- Morate uneti tip vozila\n";
				ok = false;
			}
			
		}
		
		if(ok == false) {
			JOptionPane.showMessageDialog(null, poruka, "Neispravni podaci", JOptionPane.WARNING_MESSAGE);
		}
		return ok;
	}
}
