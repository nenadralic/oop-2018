package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import guiIzmenaiDodavanje.NarudzbineFormaDostavljac;
import guiPrikaz.PrikazKorisnikaProzor;
import guiPrikaz.PrikazPorudzbinaDostavljac;
import guiPrikaz.PrikazPorudzbinaKupac;
import korisnici.Korisnici;
import restoran.Restoran;

public class GlavniProzorDostavljac extends JFrame {
	private Restoran restoran;
	private Korisnici prijavljeniKorisnik;
	
	private JMenuBar mainMenu;
	private JMenu meniMenu;
	private JMenuItem porudzbineItem;
	private JMenuItem odjavaItem;
	
	
	public GlavniProzorDostavljac(Restoran restoran, Korisnici prijavljeniKorisnik) {
		this.restoran = restoran;
		this.prijavljeniKorisnik = prijavljeniKorisnik;
		setTitle("Restoran - " + prijavljeniKorisnik.getIme()+" "+prijavljeniKorisnik.getPrezime() + " - "+ prijavljeniKorisnik.getZanimanje());
		setSize(500, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		initMenu();
		initActions();
	}
	private void initMenu() {
		this.mainMenu = new JMenuBar();
		this.meniMenu = new JMenu("Menu");
		this.porudzbineItem = new JMenuItem("Porudzbine");
		this.odjavaItem = new JMenuItem("Odjava");
		setJMenuBar(this.mainMenu);
		
		this.meniMenu.add(porudzbineItem);
		this.meniMenu.add(odjavaItem);
		
		this.mainMenu.add(meniMenu);
		setJMenuBar(this.mainMenu);
	}
	private void initActions() {
		
		porudzbineItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				PrikazPorudzbinaDostavljac pK = new PrikazPorudzbinaDostavljac(restoran, prijavljeniKorisnik);
				pK.setVisible(true);
			}
		});
		odjavaItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GlavniProzorDostavljac.this.setVisible(false);
				GlavniProzorDostavljac.this.dispose();
				LoginProzor login = new LoginProzor(restoran);
				login.setVisible(true);
			}
		});
		
	}
}
