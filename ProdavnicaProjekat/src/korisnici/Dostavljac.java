package korisnici;

public class Dostavljac extends Korisnici {
	private String jmbg;
	private double plata;
	private String tablice;
	private String vozilo;
	
	public Dostavljac() {
		this.jmbg = "";
		this.plata = 200;
		this.tablice = "";
		this.vozilo = "";
	}

	public Dostavljac(String zanimanje, String ime, String prezime, Pol pol, String korIme, String lozinka, String jmbg,
			double plata, String tablice, String vozilo) {
		super(zanimanje, ime, prezime, pol, korIme, lozinka);
		this.jmbg = jmbg;
		this.plata = plata;
		this.tablice = tablice;
		this.vozilo = vozilo;
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public double getPlata() {
		return plata;
	}

	public void setPlata(double plata) {
		this.plata = plata;
	}

	public String getTablice() {
		return tablice;
	}

	public void setTablice(String tablice) {
		this.tablice = tablice;
	}

	public String getVozilo() {
		return vozilo;
	}

	public void setVozilo(String vozilo) {
		this.vozilo = vozilo;
	}

	@Override
	public String toString() {
		return "Dostavljac [jmbg=" + jmbg + ", plata=" + plata + ", tablice=" + tablice + ", vozilo=" + vozilo
				+ ", zanimanje=" + zanimanje + ", ime=" + ime + ", prezime=" + prezime + ", pol=" + pol + ", korIme="
				+ korIme + ", lozinka=" + lozinka + "]";
	}

	

	
	
}
