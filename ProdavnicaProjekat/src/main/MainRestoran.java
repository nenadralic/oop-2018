package main;

import javax.swing.JOptionPane;

import gui.LoginProzor;
import restoran.Restoran;

public class MainRestoran {
	public static void main(String[] args) {
		
				Restoran restoran = new Restoran();
				restoran.ucitajKorisnike();
				restoran.ucitajRestorane();
				restoran.ucitajNarudzbine();
				restoran.ucitajArtikle();
				

				LoginProzor login = new LoginProzor(restoran);
				login.setVisible(true);
	}
}
