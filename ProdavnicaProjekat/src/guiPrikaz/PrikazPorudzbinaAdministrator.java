package guiPrikaz;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import artikli.Artikli;
import artikli.Narudzbina;
import guiIzmenaiDodavanje.ArtikliForma;
import guiIzmenaiDodavanje.NarudzbineFormaAdministrator;
import guiIzmenaiDodavanje.NarudzbineFormaAdministratorPostavljanjeDostavljaca;
import guiIzmenaiDodavanje.NarudzbineFormaKupac;
import korisnici.Dostavljac;
import korisnici.Korisnici;
import restoran.Restoran;

public class PrikazPorudzbinaAdministrator extends JFrame {
	private Korisnici prijavljeniKorisnik;
	private Restoran restoran;
	private Dostavljac dostavljaci;
	private Narudzbina narudzbina;
	
	
	private JLabel lblJelo = new JLabel("Pol");
	private JToolBar mainToolbar = new JToolBar();
	private JButton btnAdd = new JButton();
	private JButton btnEdit = new JButton();
	private JButton btnDelete = new JButton();
	
	private DefaultTableModel tableModel;
	private JTable narudzbineTabela;
	
	public PrikazPorudzbinaAdministrator(Restoran restoran, Korisnici prijavljeniKorisnik) {
		this.restoran = restoran;
		this.prijavljeniKorisnik = prijavljeniKorisnik;
		setTitle("Narudzbine");
		setSize(300, 300);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		initGUI();
		initActions();
	}
	private void initGUI() {
		ImageIcon addIcon = new ImageIcon(getClass().getResource("/slike/add.png"));
		btnAdd.setIcon(addIcon);
		mainToolbar.add(btnAdd);
		ImageIcon editIcon = new ImageIcon(getClass().getResource("/slike/edit.png"));
		btnEdit.setIcon(editIcon);
		mainToolbar.add(btnEdit);
		add(mainToolbar, BorderLayout.NORTH);
		
		String[] zaglavlje = new String[] {"Jelo" ,"Pice", "Datum", "Kupac", "Dostavljac", "Cena", "Status"};
		Object[][] podaci = new Object[this.restoran.getNarudzbine().size()][zaglavlje.length];
		
		for(int i=0; i<this.restoran.getNarudzbine().size(); i++) {
			Narudzbina narudzbine = this.restoran.getNarudzbine().get(i);
			podaci[i][0] = narudzbine.getJelo();
			podaci[i][1] = narudzbine.getPice();
			podaci[i][2] = narudzbine.getVremeiDatum();
			podaci[i][3] = narudzbine.getKupac();
			podaci[i][4] = narudzbine.getDostavljac();
			podaci[i][5] = narudzbine.getCenaPorudzbine();
			podaci[i][6] = narudzbine.getStatus();
			
		}
	
		
		tableModel = new DefaultTableModel(podaci, zaglavlje);
		narudzbineTabela = new JTable(tableModel);
		narudzbineTabela = new JTable(tableModel);
		narudzbineTabela.setRowSelectionAllowed(true);
		narudzbineTabela.setColumnSelectionAllowed(false);
		narudzbineTabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		narudzbineTabela.setDefaultEditor(Object.class, null);
		
		JScrollPane scrollPane = new JScrollPane(narudzbineTabela);
		add(scrollPane, BorderLayout.CENTER);
	}
	private void initActions() {
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				NarudzbineFormaAdministrator zf = new NarudzbineFormaAdministrator(restoran, prijavljeniKorisnik);
				zf.setVisible(true);
			}
		});
	
		btnEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int red = narudzbineTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate odabrati red u tabeli.", "Greska", JOptionPane.WARNING_MESSAGE);
				}else {
					DefaultTableModel model = (DefaultTableModel)narudzbineTabela.getModel();
					String provera = "";
					String dostavljac = model.getValueAt(red, 4).toString();
					String status = model.getValueAt(red, 6).toString();
					Narudzbina narudzbina = restoran.nadjiNarudzbineDostavljac(dostavljac);
					if(dostavljac.equals("")) {
						if(status.equals("Odbijena") || status.equals("Otkazana") || status.equals("Dostava u toku") || status.equals("Dostavljena")) {
							
								JOptionPane.showMessageDialog(null, "Nije moguce izmena statusa!", "Greska", JOptionPane.ERROR_MESSAGE);
							
						}else {
					if(narudzbina != null ) {
						NarudzbineFormaAdministratorPostavljanjeDostavljaca zf = new NarudzbineFormaAdministratorPostavljanjeDostavljaca(restoran, narudzbina);
						zf.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "Nije moguce pronaci odabranog dostavljaca!", "Greska", JOptionPane.ERROR_MESSAGE);
					}
						}
					}else {
						JOptionPane.showMessageDialog(null, "Neko vec preuzeo porudzbinu!", "Greska", JOptionPane.ERROR_MESSAGE);

					}
				}
			}
		});
		
		
	}
	
}