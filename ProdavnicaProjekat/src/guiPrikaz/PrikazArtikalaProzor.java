package guiPrikaz;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import artikli.Artikli;
import guiIzmenaiDodavanje.ArtikliForma;
import guiIzmenaiDodavanje.KorisniciForma;
import korisnici.Korisnici;
import korisnici.Kupac;
import restoran.Restoran;

public class PrikazArtikalaProzor extends JFrame {
	private JToolBar mainToolbar = new JToolBar();
	private JButton btnAdd = new JButton();
	private JButton btnEdit = new JButton();
	private JButton btnDelete = new JButton();
	
	private DefaultTableModel tableModel;
	private JTable artikliTabela;
	
	private Restoran restoran;
	
	public PrikazArtikalaProzor(Restoran restoran) {
		this.restoran = restoran;
		setTitle("Artikli");
		setSize(300, 300);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		initGUI();
		initActions();
	}
	private void initGUI() {
		ImageIcon addIcon = new ImageIcon(getClass().getResource("/slike/add.png"));
		btnAdd.setIcon(addIcon);
		mainToolbar.add(btnAdd);
		ImageIcon editIcon = new ImageIcon(getClass().getResource("/slike/edit.png"));
		btnEdit.setIcon(editIcon);
		mainToolbar.add(btnEdit);
		ImageIcon deleteIcon = new ImageIcon(getClass().getResource("/slike/remove.png"));
		btnDelete.setIcon(deleteIcon);
		mainToolbar.add(btnDelete);
		add(mainToolbar, BorderLayout.NORTH);
		
		String[] zaglavlje = new String[] {"Vrsta" ,"Restoran", "Naziv", "Cena", "Opis", "Kolicina"};
		Object[][] podaci = new Object[this.restoran.getArtikli().size()][zaglavlje.length];
		
		for(int i=0; i<this.restoran.getArtikli().size(); i++) {
			Artikli artikl = this.restoran.getArtikli().get(i);
			podaci[i][0] = artikl.getJeloPice();
			podaci[i][1] = artikl.getRestoran();
			podaci[i][2] = artikl.getJelo();
			podaci[i][3] = artikl.getCena();
			podaci[i][4] = artikl.getVrsta();
			podaci[i][5] = artikl.getKolicina();
			
		}
		tableModel = new DefaultTableModel(podaci, zaglavlje);
		artikliTabela = new JTable(tableModel);
		artikliTabela = new JTable(tableModel);
		artikliTabela.setRowSelectionAllowed(true);
		artikliTabela.setColumnSelectionAllowed(false);
		artikliTabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		artikliTabela.setDefaultEditor(Object.class, null);
		
		JScrollPane scrollPane = new JScrollPane(artikliTabela);
		add(scrollPane, BorderLayout.CENTER);
		
	}
	private void initActions() {
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ArtikliForma zf = new ArtikliForma(restoran, null);
				zf.setVisible(true);
			}
		});
		btnEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int red = artikliTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate odabrati red u tabeli.", "Greska", JOptionPane.WARNING_MESSAGE);
				}else {
					DefaultTableModel model = (DefaultTableModel)artikliTabela.getModel();
					String naziv = model.getValueAt(red, 2).toString();
					Artikli artikal = restoran.nadjiArtikle(naziv);
					if(artikal != null) {
						ArtikliForma zf = new ArtikliForma(restoran, artikal);
						zf.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "Nije moguce pronaci odabranog prodavca!", "Greska", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btnDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int red = artikliTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate odabrati red u tabeli.", "Greska", JOptionPane.WARNING_MESSAGE);
				}else {
					DefaultTableModel model = (DefaultTableModel)artikliTabela.getModel();
					String naziv = model.getValueAt(red, 2).toString();
					Artikli artikal = restoran.nadjiArtikle(naziv);
					if(naziv != null) {
						int izbor = JOptionPane.showConfirmDialog(null, "Da li ste sigurni da zelite da obrisete jelo", artikal.getJelo() + " - Potvrda brisanja", JOptionPane.YES_NO_OPTION);
						if(izbor == JOptionPane.YES_OPTION) {
							restoran.getArtikli().remove(naziv);
							model.removeRow(red);
							restoran.snimiArtikle();;
						}
					}else {
						JOptionPane.showMessageDialog(null, "Nije moguce pronaci odabranog prodavca!", "Greska", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
	}
	
}
