package artikli;

public class Narudzbina {
	private String jelo;
	private String pice;
	private String vremeiDatum;
	private String kupac;
	private String dostavljac;
	private double cenaPorudzbine;
	private String status;
	
	public Narudzbina() {
		this.jelo = "";
		this.pice = "";
		this.vremeiDatum = "";
		this.kupac = "";
		this.dostavljac = "";
		this.cenaPorudzbine = 200;
		this.status = "";
	}

	public Narudzbina(String jelo, String pice, String vremeiDatum, String kupac, String dostavljac,
			double cenaPorudzbine, String status) {
		super();
		this.jelo = jelo;
		this.pice = pice;
		this.vremeiDatum = vremeiDatum;
		this.kupac = kupac;
		this.dostavljac = dostavljac;
		this.cenaPorudzbine = cenaPorudzbine;
		this.status = status;
	}

	public String getJelo() {
		return jelo;
	}

	public void setJelo(String jelo) {
		this.jelo = jelo;
	}

	public String getPice() {
		return pice;
	}

	public void setPice(String pice) {
		this.pice = pice;
	}

	public String getVremeiDatum() {
		return vremeiDatum;
	}

	public void setVremeiDatum(String vremeiDatum) {
		this.vremeiDatum = vremeiDatum;
	}

	public String getKupac() {
		return kupac;
	}

	public void setKupac(String kupac) {
		this.kupac = kupac;
	}

	public String getDostavljac() {
		return dostavljac;
	}

	public void setDostavljac(String dostavljac) {
		this.dostavljac = dostavljac;
	}

	public double getCenaPorudzbine() {
		return cenaPorudzbine;
	}

	public void setCenaPorudzbine(double cenaPorudzbine) {
		this.cenaPorudzbine = cenaPorudzbine;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Narudzbina [jelo=" + jelo + ", pice=" + pice + ", vremeiDatum=" + vremeiDatum + ", kupac=" + kupac
				+ ", dostavljac=" + dostavljac + ", cenaPorudzbine=" + cenaPorudzbine + ", status=" + status + "]";
	}

	
	
}
