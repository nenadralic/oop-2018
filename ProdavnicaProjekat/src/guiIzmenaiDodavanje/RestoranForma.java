package guiIzmenaiDodavanje;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import artikli.Artikli;
import artikli.RestoranLista;
import korisnici.Korisnici;
import net.miginfocom.swing.MigLayout;
import restoran.Restoran;

public class RestoranForma extends JFrame {
	private JLabel lblNaziv = new JLabel("Naziv");
	private JTextField txtNaziv = new JTextField(20);
	private JLabel lblAdresa = new JLabel("Adresa");
	private JTextField txtAdresa = new JTextField(20);
	private JLabel lblVrstaKuhinje = new JLabel("Vrsta kuhinje");
	private JTextField txtVrstaKuhinje = new JTextField(20);
	
	private JButton btnOk = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	
	private Restoran restoran;
	private Korisnici korisnik;
	private Artikli artikal;
	private RestoranLista restoranLista;
	
	
	public RestoranForma(Restoran restoran, RestoranLista restoranLista) {
		this.restoran = restoran;
		this.korisnik = korisnik;
		this.artikal = artikal;
		this.restoranLista = restoranLista;
		if(this.korisnik == null) {
			setTitle("Dodavanje artikala");
		}else {
			setTitle("Izmena podataka - " + this.korisnik.getKorIme());
		}
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		initGUI();
		initActions();
		setResizable(false);
		pack();
	}
	
	private void initGUI() {
		MigLayout layout = new MigLayout("wrap 2");
		setLayout(layout);
		
		if(this.korisnik != null) {
			popuniPolja();
		}
		if(this.restoranLista != null) {
			txtNaziv.setText(this.restoranLista.getRestoran());
			txtAdresa.setText(this.restoranLista.getAdresa());
			txtVrstaKuhinje.setText(this.restoranLista.getVrstaKuhinje());
		}
		
		
		add(lblNaziv);
		add(txtNaziv);
		add(lblAdresa);
		add(txtAdresa);
		add(lblVrstaKuhinje);
		add(txtVrstaKuhinje);
		
		
		add(new JLabel());
		add(btnOk, "split 2");
		add(btnCancel);
	}
	
	private void initActions() {
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(validacija() == true) {
					String naziv = txtNaziv.getText().trim();
					String adresa = txtAdresa.getText().trim();
					String vrstaKuhinje = txtVrstaKuhinje.getText().trim();
					if(restoranLista == null) {
						RestoranLista restoranListe = new RestoranLista(naziv, adresa, vrstaKuhinje);
						restoran.getRestoranLista().add(restoranListe);
							
							
							
						}else {
							RestoranLista restoranListe = (RestoranLista) restoranLista;
							restoranListe.setRestoran(naziv);
							restoranListe.setAdresa(adresa);
							restoranListe.setVrstaKuhinje(vrstaKuhinje);
							
						}
					
					}
					restoran.snimiRestorane();
					RestoranForma.this.dispose();
					RestoranForma.this.setVisible(false);
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				RestoranForma.this.dispose();
				RestoranForma.this.setVisible(false);
			}
		});
	}

	
	
	private void popuniPolja() {
		txtNaziv.setText(this.restoranLista.getRestoran());
		txtAdresa.setText(String.valueOf(this.restoranLista.getAdresa()));
		txtVrstaKuhinje.setText(this.restoranLista.getVrstaKuhinje());
		
	}
	
	private boolean validacija() {
		boolean ok = true;
		String poruka = "Molimo popravite sledece greske u unosu:\n";
		
		if(txtNaziv.getText().trim().equals("")) {
			poruka += "- Morate uneti naziv\n";
			ok = false;
		}
		if(txtAdresa.getText().trim().equals("")) {
			poruka += "- Morate uneti adresu\n";
			ok = false;
		}
		if(txtVrstaKuhinje.getText().trim().equals("")) {
			poruka += "- Morate uneti vrstu kuhinje\n";
			ok = false;
		}
		
		if(ok == false) {
			JOptionPane.showMessageDialog(null, poruka, "Neispravni podaci", JOptionPane.WARNING_MESSAGE);
		}
		return ok;
	}
	
}
