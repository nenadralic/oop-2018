package korisnici;

public class Kupac extends Korisnici {
	private String brTelefona;
	private String adresa;
	
	public Kupac() {
		this.brTelefona = "";
		this.adresa = "";
	}

	
	public Kupac(String zanimanje, String ime, String prezime, Pol pol, String korIme, String lozinka,
			String brTelefona, String adresa) {
		super(zanimanje, ime, prezime, pol, korIme, lozinka);
		this.brTelefona = brTelefona;
		this.adresa = adresa;
	}


	public String getBrTelefona() {
		return brTelefona;
	}

	public void setBrTelefona(String brTelefona) {
		this.brTelefona = brTelefona;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	@Override
	public String toString() {
		return "Kupac [brTelefona=" + brTelefona + ", adresa=" + adresa + ", zanimanje=" + zanimanje + ", ime=" + ime
				+ ", prezime=" + prezime + ", pol=" + pol + ", korIme=" + korIme + ", lozinka=" + lozinka + "]";
	}

	
	
}
