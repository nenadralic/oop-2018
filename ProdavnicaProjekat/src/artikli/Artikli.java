package artikli;

public class Artikli {
	private String jeloPice;
	private String restoran;
	private String jelo;
	private double cena;
	private String vrsta;
	private double kolicina;
	
	public Artikli() {
		this.jeloPice = "";
		this.restoran = "";
		this.jelo = "";
		this.cena = 250;
		this.vrsta = "";
		this.kolicina = 300;
	}

	public Artikli(String jeloPice, String restoran, String jelo, double cena, String vrsta, double kolicina) {
		super();
		this.jeloPice = jeloPice;
		this.restoran = restoran;
		this.jelo = jelo;
		this.cena = cena;
		this.vrsta = vrsta;
		this.kolicina = kolicina;
	}

	public String getJeloPice() {
		return jeloPice;
	}

	public void setJeloPice(String jeloPice) {
		this.jeloPice = jeloPice;
	}

	public String getRestoran() {
		return restoran;
	}

	public void setRestoran(String restoran) {
		this.restoran = restoran;
	}

	public String getJelo() {
		return jelo;
	}

	public void setJelo(String jelo) {
		this.jelo = jelo;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public String getVrsta() {
		return vrsta;
	}

	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}

	public double getKolicina() {
		return kolicina;
	}

	public void setKolicina(double kolicina) {
		this.kolicina = kolicina;
	}

	@Override
	public String toString() {
		return "Artikli [jeloPice=" + jeloPice + ", restoran=" + restoran + ", jelo=" + jelo + ", cena=" + cena
				+ ", vrsta=" + vrsta + ", kolicina=" + kolicina + "]";
	}
	
	
}
