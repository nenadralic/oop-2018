package guiIzmenaiDodavanje;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import artikli.Artikli;
import artikli.Narudzbina;
import korisnici.Dostavljac;
import korisnici.Korisnici;
import net.miginfocom.swing.MigLayout;
import restoran.Restoran;

public class NarudzbineFormaDostavljac extends JFrame {
	private JLabel lblJelo = new JLabel("Jelo");
	private JTextField txtJelo = new JTextField(20);
	private JLabel lblDatum = new JLabel("Datum");
	private JTextField txtDatum = new JTextField(20);
	private JLabel lblKupac = new JLabel("Kupac");
	private JTextField txtKupac = new JTextField(20);
	private JLabel lblDostavljac = new JLabel("Dostavljac");
	private JTextField txtDostavljac = new JTextField(20);
	private JLabel lblCena = new JLabel("Cena");
	private JTextField txtCena = new JTextField(20);
	private JLabel lblStatus = new JLabel("Status");
	private JComboBox<String> cbStatus = new JComboBox<String>();
	private JLabel lblPice = new JLabel("Pice");
	private JTextField txtPice = new JTextField(20);
	
	private JButton btnOk = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	
	private Restoran restoran;
	private Korisnici korisnik;
	private Artikli artikal;
	private Narudzbina narudzbine;
	
	public NarudzbineFormaDostavljac(Restoran restoran, Narudzbina narudzbine, Korisnici korisnik) {
		this.restoran = restoran;
		this.korisnik = korisnik;
		this.narudzbine = narudzbine;
		if(this.korisnik == null) {
			setTitle("Narudzbine");
		}else {
			setTitle("Izmena podataka - " + this.korisnik.getKorIme());
		}
	//	JOptionPane.showMessageDialog(null, this.korisnik.getKorIme());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		initGUI();
		initActions();
		setResizable(false);
		pack();
	}
	
	private void initGUI() {
		MigLayout layout = new MigLayout("wrap 2");
		setLayout(layout);
		
		if(this.korisnik != null) {
			popuniPolja();
		}
		
		
		enablePolja(true);
		if(this.narudzbine != null) {
			txtJelo.setText(this.narudzbine.getJelo());
			txtPice.setText(this.narudzbine.getPice());
			txtDatum.setText(this.narudzbine.getVremeiDatum());
			txtKupac.setText(this.narudzbine.getKupac());
			txtDostavljac.setText(korisnik.getKorIme());
			txtCena.setText(String.valueOf(this.narudzbine.getCenaPorudzbine()));
			
		
		}
		cbStatus.addItem("Dostava u toku");
		cbStatus.addItem("Dostavljeno");
		cbStatus.addItem("Odbijeno");
		
		add(lblJelo);
		add(txtJelo);
		add(lblPice);
		add(txtPice);
		add(lblDatum);
		add(txtDatum);
		add(lblKupac);
		add(txtKupac);
		add(lblDostavljac);
		add(txtDostavljac);
		add(lblCena);
		add(txtCena);
		add(lblStatus);
		add(cbStatus);
		
		
		
		
		add(new JLabel());
		add(btnOk, "split 2");
		add(btnCancel);
	}
	private void initActions() {
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(validacija() == true) {
					
				
					String jelo = txtJelo.getText().trim();
					String pice = txtPice.getText().trim();
					String datum = txtDatum.getText().trim();
					String kupac = txtKupac.getText().trim();
					double cena = Double.parseDouble(txtCena.getText().trim());
					String dostavljac = txtDostavljac.getText().trim();
					String status = cbStatus.getSelectedItem().toString();
					if(narudzbine != null) {
						Narudzbina narudzbina = (Narudzbina) narudzbine;
						narudzbina.setJelo(jelo);
						narudzbina.setPice(pice);
						narudzbina.setVremeiDatum(datum);
						narudzbina.setKupac(kupac);
						narudzbina.setDostavljac(dostavljac);
						narudzbina.setCenaPorudzbine(cena);
						narudzbina.setStatus(status);
						
						}else {
							Narudzbina narudzbina = new Narudzbina(jelo, pice, datum, kupac, dostavljac, cena, status);
							restoran.getNarudzbine().add(narudzbina);
							
						}
					
			}
					restoran.snimiNarudzbine();
					NarudzbineFormaDostavljac.this.dispose();
					NarudzbineFormaDostavljac.this.setVisible(false);
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				NarudzbineFormaDostavljac.this.dispose();
				NarudzbineFormaDostavljac.this.setVisible(false);
			}
		});
	}
	private void enablePolja(boolean enable) {
		txtJelo.setEnabled(!enable);
		txtPice.setEnabled(!enable);
		txtDatum.setEnabled(!enable);
		txtKupac.setEnabled(!enable);
		txtCena.setEnabled(!enable);
		txtDostavljac.setEnabled(!enable);
	}
	private void popuniPolja() {
		cbStatus.setSelectedItem(this.narudzbine.getDostavljac());
		
	}
	private boolean validacija() {
		boolean ok = true;
		String poruka = "Molimo popravite sledece greske u unosu:\n";
		
		
		if(ok == false) {
			JOptionPane.showMessageDialog(null, poruka, "Neispravni podaci", JOptionPane.WARNING_MESSAGE);
		}
		return ok;
	}
}
