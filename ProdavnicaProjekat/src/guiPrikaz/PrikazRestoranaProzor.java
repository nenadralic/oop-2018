package guiPrikaz;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import artikli.Artikli;
import artikli.RestoranLista;
import guiIzmenaiDodavanje.ArtikliForma;
import guiIzmenaiDodavanje.RestoranForma;
import restoran.Restoran;

public class PrikazRestoranaProzor extends JFrame {
	private JToolBar mainToolbar = new JToolBar();
	private JButton btnAdd = new JButton();
	private JButton btnEdit = new JButton();
	private JButton btnDelete = new JButton();
	
	private DefaultTableModel tableModel;
	private JTable restoranaTabela;
	
	private Restoran restoran;
	
	public PrikazRestoranaProzor(Restoran restoran) {
		this.restoran = restoran;
		setTitle("Restorani");
		setSize(300, 300);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		initGUI();
		initActions();
		
	}
	private void initGUI() {
		ImageIcon addIcon = new ImageIcon(getClass().getResource("/slike/add.png"));
		btnAdd.setIcon(addIcon);
		mainToolbar.add(btnAdd);
		ImageIcon editIcon = new ImageIcon(getClass().getResource("/slike/edit.png"));
		btnEdit.setIcon(editIcon);
		mainToolbar.add(btnEdit);
		ImageIcon deleteIcon = new ImageIcon(getClass().getResource("/slike/remove.png"));
		btnDelete.setIcon(deleteIcon);
		mainToolbar.add(btnDelete);
		add(mainToolbar, BorderLayout.NORTH);
		
		String[] zaglavlje = new String[] {"Restoran" ,"Adresa", "Vrsta kuhinje"};
		Object[][] podaci = new Object[this.restoran.getRestoranLista().size()][zaglavlje.length];
		
		for(int i=0; i<this.restoran.getRestoranLista().size(); i++) {
			RestoranLista restorani = this.restoran.getRestoranLista().get(i);
			podaci[i][0] = restorani.getRestoran();
			podaci[i][1] = restorani.getAdresa();
			podaci[i][2] = restorani.getVrstaKuhinje();
		}
		tableModel = new DefaultTableModel(podaci, zaglavlje);
		restoranaTabela = new JTable(tableModel);
		restoranaTabela = new JTable(tableModel);
		restoranaTabela.setRowSelectionAllowed(true);
		restoranaTabela.setColumnSelectionAllowed(false);
		restoranaTabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		restoranaTabela.setDefaultEditor(Object.class, null);
		
		JScrollPane scrollPane = new JScrollPane(restoranaTabela);
		add(scrollPane, BorderLayout.CENTER);
		
	}
	private void initActions() {
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RestoranForma zf = new	RestoranForma(restoran, null);
				zf.setVisible(true);
			}
		});
		btnEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int red = restoranaTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate odabrati red u tabeli.", "Greska", JOptionPane.WARNING_MESSAGE);
				}else {
					DefaultTableModel model = (DefaultTableModel)restoranaTabela.getModel();
					String naziv = model.getValueAt(red, 0).toString();
					RestoranLista restorani = restoran.nadjiRestoran(naziv);
					if(restorani != null) {
						RestoranForma zf = new RestoranForma(restoran, restorani);
						zf.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "Nije moguce pronaci odabranog prodavca!", "Greska", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btnDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int red = restoranaTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate odabrati red u tabeli.", "Greska", JOptionPane.WARNING_MESSAGE);
				}else {
					DefaultTableModel model = (DefaultTableModel)restoranaTabela.getModel();
					String naziv = model.getValueAt(red, 0).toString();
					RestoranLista restorani = restoran.nadjiRestoran(naziv);
					if(restorani != null) {
						int izbor = JOptionPane.showConfirmDialog(null, "Da li ste sigurni da zelite da obrisete restoran", restorani.getRestoran() + " - Potvrda brisanja", JOptionPane.YES_NO_OPTION);
						if(izbor == JOptionPane.YES_OPTION) {
							restoran.getArtikli().remove(naziv);
							model.removeRow(red);
							restoran.snimiArtikle();;
						}
					}else {
						JOptionPane.showMessageDialog(null, "Nije moguce pronaci odabranog prodavca!", "Greska", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
	}
	
}


