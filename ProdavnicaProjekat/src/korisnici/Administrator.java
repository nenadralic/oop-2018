package korisnici;

public class Administrator extends Korisnici {
	private String jmbg;
	private String plata;
	
	public Administrator() {
		this.jmbg = "";
		this.plata = "";
	}


	public Administrator(String zanimanje, String ime, String prezime, Pol pol, String korIme, String lozinka,
			String jmbg, String plata) {
		super(zanimanje, ime, prezime, pol, korIme, lozinka);
		this.jmbg = jmbg;
		this.plata = plata;
	}





	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getPlata() {
		return plata;
	}

	public void setPlata(String plata) {
		this.plata = plata;
	}

	@Override
	public String toString() {
		return "Administrator [jmbg=" + jmbg + ", plata=" + plata + ", zanimanje=" + zanimanje + ", ime=" + ime
				+ ", prezime=" + prezime + ", pol=" + pol + ", korIme=" + korIme + ", lozinka=" + lozinka + "]";
	}

	
}
