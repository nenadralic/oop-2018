package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;
import korisnici.Korisnici;
import restoran.Restoran;

public class LoginProzor extends JFrame {
	private JLabel lblPoruka;
	private JLabel lblKorisnickoIme;
	private JTextField txtKorisnickoIme;
	private JLabel lblSifra;
	private JPasswordField pfSifra;
	private JButton btnOK;
	private JButton btnCancel;
	
	private Restoran restoran;
	
	public LoginProzor(Restoran restoran) {
		this.restoran = restoran;
		setTitle("Prijava");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		initGUI();
		initActions();
		pack();
	}
	
	private void initGUI() {
		/*
		 * 	Malo detaljnije podesavanje MigLayout-a:
		 * 	Drugi parametar (string) sadrzi 2 prazne uglaste zagrade jer imamo 2 kolone (ovde nista nismo podesili)
		 *  Treci parametar ima onoliko uglastih zagrada koliko imamo redova (u nasem slucaju 4)
		 *  Unutar zagrada mozemo detaljnije podesavati kolone i redove, dok vrednosti izmedju njih predstavljaju
		 *  razmake u pikselima.
		 *  Ovde smo postavili razmak od 20px izmedju 1. i 2. i izmedju 3. i 4. reda.
		 */
		MigLayout layout = new MigLayout("wrap 2", "[][]", "[]20[][]20[]");
		setLayout(layout);
		
		this.lblPoruka = new JLabel("Dobrodosli. Molimo da se prijavite.");
		this.lblKorisnickoIme = new JLabel("Korisnicko ime");
		this.txtKorisnickoIme = new JTextField(20);
		this.lblSifra = new JLabel("Sifra");
		this.pfSifra = new JPasswordField(20);
		this.btnOK = new JButton("OK");
		this.btnCancel = new JButton("Cancel");
		
		// Ako postavimo dugme 'btnOK' kao defaul button, onda ce svaki pritisak tastera Enter na tastaturi
		// Izazvati klik na njega
		this.getRootPane().setDefaultButton(btnOK);
		
		add(lblPoruka, "span 2");
		add(lblKorisnickoIme);
		add(txtKorisnickoIme);
		add(lblSifra);
		add(pfSifra);
		add(new JLabel());
		add(btnOK, "split 2");
		add(btnCancel);
	}
	
	private void initActions() {
		// Klik na Login dugme
		btnOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String korisnickoIme = txtKorisnickoIme.getText().trim();
				String sifra = new String(pfSifra.getPassword()).trim();
				// Ukoliko nesto nije uneseno, obavestimo korisnika
				if(korisnickoIme.equals("") || sifra.equals("")) {
					JOptionPane.showMessageDialog(null, "Niste uneli sve podatke.");
				}else {

					Korisnici korisnik = restoran.login(korisnickoIme, sifra);
					if(korisnik != null ) {
						JOptionPane.showMessageDialog(null, restoran.getKupac());

						if(korisnik.getZanimanje().equals("Kupac")) {
								LoginProzor.this.setVisible(false);
								LoginProzor.this.dispose();
								GlavniProzorKupac glavni = new GlavniProzorKupac(restoran, korisnik);
								glavni.setVisible(true);
								
						}else if(korisnik.getZanimanje().equals("Administrator")) {
							LoginProzor.this.setVisible(false);
							LoginProzor.this.dispose();
							GlavniProzorAdministrator glavniD = new GlavniProzorAdministrator(restoran, korisnik);
							glavniD.setVisible(true);
							
						}else if(korisnik.getZanimanje().equals("Dostavljac")) {
							LoginProzor.this.setVisible(false);
							LoginProzor.this.dispose();
							GlavniProzorDostavljac glavniD = new GlavniProzorDostavljac(restoran, korisnik);
							glavniD.setVisible(true);
							
						}
					}else {
						JOptionPane.showMessageDialog(null, "Pogresni login podaci!");
					}
				}
			}
		});
		// Cancel dugme samo sakriva trenutni prozor
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LoginProzor.this.setVisible(false);
				LoginProzor.this.dispose();
			}
		});
		
	}
}
