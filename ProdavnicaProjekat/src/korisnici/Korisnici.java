package korisnici;

public abstract class Korisnici {
	public String zanimanje;
	public String ime;
	public String prezime;
	public Pol pol;
	public String korIme;
	public String lozinka;
	
	public Korisnici() {
		this.zanimanje = "";
		this.ime = "" ;
		this.prezime = "";
		this.pol = Pol.MUSKI;
		this.korIme  = "";
		this.lozinka = "";
	}

	public Korisnici(String zanimanje, String ime, String prezime, Pol pol, String korIme, String lozinka) {
		super();
		this.zanimanje = zanimanje;
		this.ime = ime;
		this.prezime = prezime;
		this.pol = pol;
		this.korIme = korIme;
		this.lozinka = lozinka;
	}

	public String getZanimanje() {
		return zanimanje;
	}

	public void setZanimanje(String zanimanje) {
		this.zanimanje = zanimanje;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Pol getPol() {
		return pol;
	}

	public void setPol(Pol pol) {
		this.pol = pol;
	}

	public String getKorIme() {
		return korIme;
	}

	public void setKorIme(String korIme) {
		this.korIme = korIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	@Override
	public String toString() {
		return "Korisnici [zanimanje=" + zanimanje + ", ime=" + ime + ", prezime=" + prezime + ", pol=" + pol
				+ ", korIme=" + korIme + ", lozinka=" + lozinka + "]";
	}
}
