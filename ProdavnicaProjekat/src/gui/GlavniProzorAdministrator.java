package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import guiPrikaz.PrikazArtikalaProzor;
import guiPrikaz.PrikazKorisnikaProzor;
import guiPrikaz.PrikazPorudzbinaAdministrator;
import guiPrikaz.PrikazRestoranaProzor;
import korisnici.Korisnici;

import restoran.Restoran;

public class GlavniProzorAdministrator extends JFrame {
	private Restoran restoran;
	private Korisnici prijavljeniKorisnik;
	
	private JMenuBar mainMenu;
	private JMenu meniMenu;
	private JMenuItem korisniciItem;
	private JMenuItem porudzbineItem;
	private JMenuItem artikliItem;
	private JMenuItem restoranItem;
	private JMenuItem odjavaItem;
	
	
	public GlavniProzorAdministrator(Restoran restoran, Korisnici prijavljeniKorisnik) {
		this.restoran = restoran;
		this.prijavljeniKorisnik = prijavljeniKorisnik;
		setTitle("Restoran - " + prijavljeniKorisnik.getIme()+" "+prijavljeniKorisnik.getPrezime() + " - "+ prijavljeniKorisnik.getZanimanje());
		setSize(500, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		initMenu();
		initActions();
	}
	private void initMenu() {
		this.mainMenu = new JMenuBar();
		this.meniMenu = new JMenu("Menu");
		this.korisniciItem = new JMenuItem("Korisnici");
		this.porudzbineItem = new JMenuItem("Porudzbine");
		this.artikliItem = new JMenuItem("Artikli");
		this.restoranItem = new JMenuItem("Restoran");
		this.odjavaItem = new JMenuItem("Odjava");
		setJMenuBar(this.mainMenu);
		
		this.meniMenu.add(korisniciItem);
		this.meniMenu.add(porudzbineItem);
		this.meniMenu.add(artikliItem);
		this.meniMenu.add(restoranItem);
		this.meniMenu.add(odjavaItem);
		
		this.mainMenu.add(meniMenu);
		setJMenuBar(this.mainMenu);
	}
	private void initActions() {
		
		korisniciItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PrikazKorisnikaProzor pK = new PrikazKorisnikaProzor(restoran);
				pK.setVisible(true);
			}
		});
		artikliItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PrikazArtikalaProzor pA = new PrikazArtikalaProzor(restoran);
				pA.setVisible(true);
			}
		});
		restoranItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PrikazRestoranaProzor pR = new PrikazRestoranaProzor(restoran);
				pR.setVisible(true);
			}
		});
		porudzbineItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PrikazPorudzbinaAdministrator pR = new PrikazPorudzbinaAdministrator(restoran, prijavljeniKorisnik);
				pR.setVisible(true);
			}
		});
		odjavaItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GlavniProzorAdministrator.this.setVisible(false);
				GlavniProzorAdministrator.this.dispose();
				LoginProzor login = new LoginProzor(restoran);
				login.setVisible(true);
			}
		});
		
		
	}
}
